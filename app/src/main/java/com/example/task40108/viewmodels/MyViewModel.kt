package com.example.task40108.viewmodels

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.task40108.App
import com.example.task40108.tools.background.BackgroundWork
import com.example.task40108.tools.background.MyCallBack
import com.example.task40108.tools.screenstates.SecondScreen
import com.example.task40108.views.SecondActivity

class MyViewModel: MyCallBack, ViewModel() {
    var screenState: SecondScreen =  App.instance.secondScreen

    fun buttonOnClick(view:View){
        val context = view.context
        context.startActivity(SecondActivity.intent(context))
    }
    fun increasePlusOne(view:View){
        prepareToIncrease()

        App.instance.startBackgroundWork(0,this)
    }
    fun increasePlusOneError(view: View){
        prepareToIncrease()

        App.instance.startBackgroundWork(-1,this)
    }
    fun showDescription(view:View){
        Toast.makeText(view.context,screenState.description.get(),Toast.LENGTH_SHORT).show()
    }

    private fun prepareToIncrease(){
        screenState.createBackup()
        screenState.enable.set(false)

        var num = 1
        if(!screenState.number.get().isNullOrBlank())
            num = screenState.number.get()!!.toInt()
        screenState.number.set((++num).toString())
    }

    override fun resultFromBackground(result: Boolean) {
        Log.d("MYTAG",result.toString())
        screenState.enable.set(true)
        if(result)
            Toast.makeText(App.instance.applicationContext,"Success",Toast.LENGTH_SHORT).show()
        else{
            Toast.makeText(App.instance.applicationContext,"Error!",Toast.LENGTH_SHORT).show()
            screenState.downgrade()
        }
    }
}