package com.example.task40108.tools.screenstates

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.example.task40108.App
import com.example.task40108.R

class SecondScreen: IScreenState {

    override val enable = ObservableBoolean(true)
    override val description
            = ObservableField(App.instance.getString(R.string.default_description))
    val number= ObservableField<String>("")

    private var backupItem: SecondScreen? = null

    override fun createBackup() {
        if(backupItem==null)
            backupItem = SecondScreen()

        backupItem!!.enable.set(enable.get())
        backupItem!!.description.set(description.get())
        backupItem!!.number.set(number.get())
    }

    override fun downgrade() {
        if(backupItem==null) return

        enable.set(backupItem!!.enable.get())
        description.set(backupItem!!.description.get())
        number.set(backupItem!!.number.get())
    }
}