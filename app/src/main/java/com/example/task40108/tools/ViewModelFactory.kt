package com.example.task40108.tools

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

fun <T: ViewModel>obtainViewModel(activity: FragmentActivity, viewModelClass:Class<T>):T{
    val factory = ViewModelProvider.AndroidViewModelFactory.getInstance(activity.application)
    return ViewModelProviders.of(activity,factory).get(viewModelClass)
}