package com.example.task40108.tools.screenstates

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

interface IScreenState {
    val enable:ObservableBoolean
    val description:ObservableField<String>

    fun createBackup()
    fun downgrade()
}