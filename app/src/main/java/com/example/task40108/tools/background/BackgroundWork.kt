package com.example.task40108.tools.background

import android.os.AsyncTask

class BackgroundWork(val callBack: MyCallBack): AsyncTask<Int,Void,Boolean>() {
    private var cancelled = false

    override fun doInBackground(vararg p0: Int?): Boolean {
        try {
            for (i in 0..10) {
                Thread.sleep(1000)
                if (cancelled) return false
            }
            return p0[0]==0
        }catch (e: InterruptedException ){
            return false
        }
    }

    override fun onPostExecute(result: Boolean?) {
        callBack.resultFromBackground(result!!)
    }

    override fun onCancelled() {
        cancelled = true
    }
}