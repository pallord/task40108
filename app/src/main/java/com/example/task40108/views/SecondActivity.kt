package com.example.task40108.views

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.task40108.viewmodels.MyViewModel
import com.example.task40108.R
import com.example.task40108.databinding.ActivitySecondBinding
import com.example.task40108.tools.obtainViewModel

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySecondBinding>(this,
            R.layout.activity_second
        )
        val viewmodel = obtainViewModel(this, MyViewModel::class.java)
        binding.vm = viewmodel
    }


    companion object {
        fun intent(context: Context)=Intent(context, SecondActivity::class.java)
    }
}
