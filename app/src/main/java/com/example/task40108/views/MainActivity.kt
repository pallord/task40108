package com.example.task40108.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.task40108.viewmodels.MyViewModel
import com.example.task40108.R
import com.example.task40108.databinding.ActivityMainBinding
import com.example.task40108.tools.obtainViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,
            R.layout.activity_main
        )
        val viewModel: MyViewModel =
            obtainViewModel(this, MyViewModel::class.java)

        binding.vm=viewModel
    }

}
