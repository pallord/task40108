package com.example.task40108

import android.app.Application
import com.example.task40108.tools.background.BackgroundWork
import com.example.task40108.tools.background.MyCallBack
import com.example.task40108.tools.screenstates.SecondScreen

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        secondScreen = SecondScreen()
    }
    lateinit var secondScreen: SecondScreen

    fun startBackgroundWork(num:Int, callBack: MyCallBack){
        BackgroundWork(callBack).execute(num)
    }

    companion object {
        lateinit var instance:App
            private set
    }
}